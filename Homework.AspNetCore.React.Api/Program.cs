internal class Program
{
    private static void Main(string[] args)
    {
        const string origin = "MyAllowSpecificOrigins";

        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.
        builder.Services.AddCors(options =>
        {
            options.AddPolicy(name: origin,
                builder =>
                {
                    builder.WithOrigins(new[] { "*" })
                        .WithHeaders(new[] { "*" })
                        .WithMethods(new[] { "*" });
                });
        });

        builder.Services.AddControllers();

        var app = builder.Build();

        // Configure the HTTP request pipeline.

        app.UseHttpsRedirection();

        app.UseCors(origin);

        app.UseAuthorization();

        app.MapControllers();

        app.Run();
    }
}