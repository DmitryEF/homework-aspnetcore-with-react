import React, { Component } from 'react';
import { Container } from 'reactstrap';

export default class Layout extends Component<any> {
  static displayName = Layout.name;

  render() {
    return (
      <div>        
        <Container tag="main">
          {this.props.children}
        </Container>
      </div>
    );
  }
}
