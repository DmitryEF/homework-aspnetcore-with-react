import { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';

export interface IAppProps{
  forecasts: [], loading: boolean;
}

export default class App extends Component<{}, IAppProps> {
  static displayName = App.name;

  constructor(props: any) {
    super(props);
    this.state = { forecasts: [], loading: true };
  }

  componentDidMount() {
    this.populateWeatherData();
  }

  static renderForecastsTable(forecasts: []) {
    return (
      <table className="table table-striped" aria-labelledby="tableLabel">
        <thead>
          <tr>
            <th>Date</th>
            <th>Temp. (C)</th>
            <th>Temp. (F)</th>
            <th>Summary</th>
          </tr>
        </thead>
        <tbody>
          {forecasts.map(forecast =>
            <tr key={forecast["date"]}>
              <td>{forecast["date"]}</td>
              <td>{forecast["temperatureC"]}</td>
              <td>{forecast["temperatureF"]}</td>
              <td>{forecast["summary"]}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
      : App.renderForecastsTable(this.state.forecasts);

    return (
      <div>
        <h1 id="tableLabel">Weather forecast</h1>
        <p>Fetching data from localhost:7172</p>
        {contents}
      </div>
    );
  }

  async populateWeatherData() {
      const response = await fetch("https://localhost:7172/weatherforecast");
      console.log(JSON.stringify(response.json));
    const data = await response.json();
    this.setState({ forecasts: data, loading: false });
  }
}
